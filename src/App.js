import avatar from "./assets/images/48.jpg";

import "./App.css";

import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="container dc-container">
      <div>
        <img src={avatar} alt="avatar" className="dc-avatar"/>
      </div>
      <div className="dc-quote">
        This is one of the best developer blogs on the planet! I read it daily to improve my skills.
      </div>
      <div>
        <span className="dc-name">
          Tammy stevens
        </span>
        <span className="dc-job">
          &nbsp; * &nbsp;Front End Developer
        </span>
      </div>
    </div>
  );
}

export default App;
